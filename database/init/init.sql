CREATE TABLE Users (
    id UUID PRIMARY KEY,
    username VARCHAR(25) NOT NULL UNIQUE CHECK(username != ''),
    password text NOT NULL CHECK(password != '')
);
CREATE TABLE UserProfile (
    id UUID PRIMARY KEY REFERENCES Users(id) ON DELETE CASCADE,
    subtitle text,
    picture_url VARCHAR(100)
);

CREATE TABLE Attachments (
    id UUID PRIMARY KEY,
    picture_url VARCHAR(100) NOT NULL CHECK(picture_url != '')
);

CREATE TABLE Chats (
    id UUID PRIMARY KEY,
    title VARCHAR(50) NOT NULL CHECK(title != '')
);
CREATE TABLE Messages (
    id UUID PRIMARY KEY,
    content text NOT NULL CHECK(content != ''),
    created_at TIMESTAMP WITH TIME ZONE,

    author_id UUID REFERENCES Users(id),
    chat_id UUID REFERENCES Chats(id)
);
CREATE TABLE Posts (
    id UUID PRIMARY KEY,
    content text NOT NULL CHECK(content != ''),
    created_at TIMESTAMP WITH TIME ZONE,
    author_id UUID REFERENCES Users(id)
);


CREATE TABLE MessageAttachmentLink (
    message_id UUID PRIMARY KEY REFERENCES Messages(id),
    attachment_id UUID REFERENCES Attachments(id)
);
CREATE TABLE PostAttachmentLink (
    post_id UUID PRIMARY KEY REFERENCES Posts(id),
    attachment_id UUID REFERENCES Attachments(id)
);
CREATE TABLE UserChatLink (
    user_id UUID PRIMARY KEY REFERENCES Users(id),
    chat_id UUID REFERENCES Chats(id)
);
CREATE TABLE ChatUserLink (
    chat_id UUID PRIMARY KEY REFERENCES Chats(id),
    user_id UUID REFERENCES Users(id)
);
CREATE TABLE ChatMessageLink (
    chat_id UUID PRIMARY KEY REFERENCES Chats(id),
    message_id UUID REFERENCES Messages(id)
);


