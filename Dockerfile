FROM golang:latest
WORKDIR /app
COPY ./server/ .
RUN go build -o main .
ENTRYPOINT ["/app/main"]