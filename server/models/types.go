package models

import "github.com/jackc/pgx/v5/pgtype"

type Users struct {
	Id       pgtype.UUID
	Username pgtype.Text
	Password pgtype.Text
}
type UserProfile struct {
	Id         pgtype.UUID
	Subtitle   pgtype.Text
	PictureUrl pgtype.Text
}
type Attachments struct {
	Id         pgtype.UUID
	PictureUrl pgtype.Text
}
type Chats struct {
	Id    pgtype.UUID
	Title pgtype.Text
}
type Messages struct {
	Id        pgtype.UUID
	Content   pgtype.Text
	CreatedAt pgtype.Timestamptz
	AuthorId  pgtype.UUID
	ChatId    pgtype.UUID
}
type Posts struct {
	Id        pgtype.UUID
	Content   pgtype.Text
	CreatedAt pgtype.Timestamptz
	AuthorId  pgtype.UUID
}
type MessageAttachmentsLink struct {
	MessageId    pgtype.UUID
	AttachmentId pgtype.UUID
}
type PostAttachmentsLink struct {
	PostId       pgtype.UUID
	AttachmentId pgtype.UUID
}
type UserChatsLink struct {
	UserId pgtype.UUID
	ChatId pgtype.UUID
}
type ChatUsersLink struct {
	ChatId string
	UserId string
}
type ChatMessagesLink struct {
	ChatId    string
	MessageId string
}
