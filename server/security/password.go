package security

import "golang.org/x/crypto/bcrypt"

func HashPassword(pwd []byte) ([]byte, error) {
	bytes, err := bcrypt.GenerateFromPassword(pwd, 14)
	return bytes, err
}

func CheckHash(pwd, hash []byte) bool {
	err := bcrypt.CompareHashAndPassword(hash, pwd)
	return err == nil
}
