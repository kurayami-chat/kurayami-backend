package security

import (
	"log"
	"os"

	"github.com/golang-jwt/jwt"
)

var jwt_secret = []byte(os.Getenv("JWT_SECRET"))

func GenJWT() (string, error) {
	secret := jwt_secret
	token := jwt.New(jwt.SigningMethodHS256)
	signedToken, err := token.SignedString(secret)
	if err != nil {
		log.Println("Error on JWT token generation")
		return "", err
	}
	return signedToken, nil
}
