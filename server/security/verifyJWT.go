package security

import (
	"net/http"

	"github.com/golang-jwt/jwt"
)

func VerifyJWT(endpointHandler func(writer http.ResponseWriter, request *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		cookie, err := request.Cookie("token")
		if err != nil {
			writer.WriteHeader(http.StatusForbidden)
			_, err := writer.Write([]byte("Token is not provided\n"))
			if err != nil {
				return
			}
			return
		}
		_, err = request.Cookie("user_id")
		if err != nil {
			writer.WriteHeader(http.StatusForbidden)
			_, err := writer.Write([]byte("User ID is not provided\n"))
			if err != nil {
				return
			}
			return
		}

		token, err := jwt.Parse(cookie.Value, func(token *jwt.Token) (interface{}, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				writer.WriteHeader(http.StatusUnauthorized)
				_, err := writer.Write([]byte("JWT method invalid\n"))
				if err != nil {
					return nil, err
				}
			}
			return jwt_secret, nil
		})

		if err != nil {
			writer.WriteHeader(http.StatusUnauthorized)
			_, err2 := writer.Write([]byte("Error parsing JWT\n"))
			if err2 != nil {
				return
			}
			return
		}

		if token.Valid {
			endpointHandler(writer, request)
		} else {
			writer.WriteHeader(http.StatusUnauthorized)
			_, err := writer.Write([]byte("Invalid token\n"))
			if err != nil {
				return
			}
			return
		}
	})
}
