package centrifugo

import (
	"encoding/json"
	"github.com/jackc/pgx/v5/pgxpool"
	"log"
	"net/http"
)

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type ErrorRes struct {
	Er Error `json:"error"`
}

func PubCentrifugoMiddleware(pool *pgxpool.Pool, w http.ResponseWriter, r *http.Request) {
	log.Println("Captured publish")
	var a ErrorRes
	a.Er.Code = 403
	a.Er.Message = "permission denied"

	json.NewEncoder(w).Encode(a)
}
