package auth

import (
	"context"
	"encoding/json"
	"kurayami-backend/server/security"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

func RegisterHandler(pool *pgxpool.Pool, w http.ResponseWriter, r *http.Request) {
	var data User
	w.Header().Set("Content-Type", "application/json")
	err := json.NewDecoder(r.Body).Decode(&data)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
	}

	hash, err := security.HashPassword([]byte(data.Password))
	if err != nil {
		log.Printf("Password hashing error: %v", err)
		return
	}
	data.Password = string(hash)
	uid := uuid.New()

	_, errDb1 := pool.Exec(context.Background(),
		`insert into Users (id, username, password) values($1, $2, $3)`,
		uid.String(),
		data.Username,
		data.Password)

	if errDb1 != nil {
		log.Println(errDb1)
		w.WriteHeader(http.StatusConflict)
		err := json.NewEncoder(w).Encode("User already exist")
		if err != nil {
			return
		}
		return
	}
	_, errDb2 := pool.Exec(context.Background(),
		`insert into UserProfile (id) values ($1)`,
		uid.String())

	if errDb2 != nil {
		log.Println(errDb2)
		_, _ = pool.Exec(context.Background(),
			`delete from users where id = $1`,
			uid.String())

		w.WriteHeader(http.StatusConflict)
		err := json.NewEncoder(w).Encode("Profile already exist")
		if err != nil {
			return
		}
		return
	}
	_, errDb3 := pool.Exec(context.Background(), `insert into UserChatLink (user_id) values ($1)`, uid.String())
	if errDb3 != nil {
		log.Println(errDb3)
		_, _ = pool.Exec(context.Background(),
			`delete from users where id = $1`,
			uid.String())
		_, _ = pool.Exec(context.Background(),
			`delete from UserProfile where id = $1`,
			uid.String())

		w.WriteHeader(http.StatusInternalServerError)
		err := json.NewEncoder(w).Encode(errDb3)
		if err != nil {
			return
		}
		return
	}

	err = json.NewEncoder(w).Encode("User created")
	if err != nil {
		return
	}

}
