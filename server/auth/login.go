package auth

import (
	"context"
	"encoding/json"
	"kurayami-backend/server/security"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/jackc/pgx/v5/pgxpool"
)

func LoginHandler(pool *pgxpool.Pool, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var incomingData User
	var hashedPassword string
	var userId pgtype.UUID
	var dataUser UserData

	err := json.NewDecoder(r.Body).Decode(&incomingData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
	}

	rows := pool.QueryRow(context.Background(), "select id, password from users where username = $1", incomingData.Username)
	err = rows.Scan(&userId, &hashedPassword)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
		return
	}

	same := security.CheckHash([]byte(incomingData.Password), []byte(hashedPassword))

	if !same {
		w.WriteHeader(http.StatusForbidden)
		err := json.NewEncoder(w).Encode("Password is incorrect")
		if err != nil {
			return
		}
		return
	}

	token, err := security.GenJWT()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
		return
	}
	now := time.Now().Local()

	byteId, err := userId.MarshalJSON()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
		return
	}
	id, err := uuid.ParseBytes(byteId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
		return
	}

	dataUser.Username = incomingData.Username
	dataUser.Id = id.String()

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   token,
		Path:    "/",
		Secure:  true,
		Expires: time.Date(now.Year(), now.Month(), now.Day()+1, now.Hour(), now.Minute(), now.Second(), 0, now.Location()),
	})
	http.SetCookie(w, &http.Cookie{
		Name:    "user_id",
		Value:   id.String(),
		Path:    "/",
		Secure:  true,
		Expires: time.Date(now.Year(), now.Month(), now.Day()+1, now.Hour(), now.Minute(), now.Second(), 0, now.Location()),
	})

	err = json.NewEncoder(w).Encode(dataUser)
	if err != nil {
		return
	}
}
