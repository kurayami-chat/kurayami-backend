package auth

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserData struct {
	Id       string `json:"id"`
	Username string `json:"username"`
}
