package posts

import (
	"context"
	"encoding/json"
	"github.com/jackc/pgx/v5/pgtype"
	"kurayami-backend/server/auth"
	"log"
	"net/http"
	"time"

	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type GETRequest struct {
	Page int `json:"page"`
}
type POSTRequest struct {
	User        auth.UserData `json:"user"`
	Content     string        `json:"content"`
	Attachments []string      `json:"attachments"`
}

type JoinedEntity struct {
	Username   pgtype.Text        `json:"username"`
	PictureUrl pgtype.Text        `json:"picture_url"`
	Content    pgtype.Text        `json:"content"`
	CreatedAt  pgtype.Timestamptz `json:"created_at"`
}

func GeneralPostsHandlerGET(pool *pgxpool.Pool, w http.ResponseWriter, r *http.Request) {
	var post []*JoinedEntity
	var req GETRequest
	w.Header().Set("Content-Type", "application/json")
	err := json.NewDecoder(r.Body).Decode(&req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
		return
	}

	offset := req.Page * 15
	query := `
	select u.username, up.picture_url, p.content, p.created_at
	from posts as p
	join users as u on u.id = p.author_id 
	join userprofile as up on up.id = u.id
	
	order by created_at
	limit 15 offset $1;
	`
	err = pgxscan.Select(context.Background(), pool, &post, query, offset)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		err = json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
		return
	}

	err = json.NewEncoder(w).Encode(post)
	if err != nil {
		return
	}
}

func GeneralPostsHandlerPOST(pool *pgxpool.Pool, w http.ResponseWriter, r *http.Request) {
	var req POSTRequest

	w.Header().Set("Content-Type", "application/json")
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		err := json.NewEncoder(w).Encode(err)
		if err != nil {
			return
		}
	}

	var username string
	query := `
		select username from users where id = $1
	`

	rows := pool.QueryRow(context.Background(), query, req.User.Id)
	errDb := rows.Scan(&username)

	if errDb != nil {
		log.Println(errDb)
		w.WriteHeader(http.StatusBadRequest)
		err := json.NewEncoder(w).Encode("Invalid user id")
		if err != nil {
			return
		}
		return
	}

	newPostId := uuid.New()
	query = `
		insert into Posts (id, content, created_at, author_id) values ($1, $2, $3, $4)
	`
	_, errDb = pool.Exec(context.Background(), query,
		newPostId,
		req.Content,
		time.Now().Format(time.DateOnly+" "+time.TimeOnly), req.User.Id)

	if errDb != nil {
		log.Println(errDb)
		w.WriteHeader(http.StatusInternalServerError)
		err := json.NewEncoder(w).Encode(errDb)
		if err != nil {
			return
		}
		return
	}

	query = `
		insert into PostAttachmentLink (post_id) values ($1)
	`

	_, errDb = pool.Exec(context.Background(), query,
		newPostId)
	if errDb != nil {
		log.Println(errDb)
		w.WriteHeader(http.StatusInternalServerError)
		err := json.NewEncoder(w).Encode(errDb)
		if err != nil {
			return
		}
		return
	}
	if len(req.Attachments) != 0 {

	}
	err = json.NewEncoder(w).Encode("Successfully posted")
	if err != nil {
		return
	}
}
