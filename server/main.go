package main

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"kurayami-backend/server/auth"
	"kurayami-backend/server/centrifugo"
	"kurayami-backend/server/fs"
	"kurayami-backend/server/posts"
	"kurayami-backend/server/security"
	"log"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v5/pgxpool"
)

func main() {
	if _, fileError := os.Stat(".env"); fileError != nil {
		log.Println("File .env not found. Continuing without .env")
	} else {
		err := godotenv.Load()
		if err != nil {
			log.Println("Failed to load .env file")
			return
		}
		log.Println("File .env loaded")
	}

	router := mux.NewRouter()

	connectionUrl := fmt.Sprintf("postgres://postgres:%s@%s:%s/%s",
		os.Getenv("PG_PASSWORD"),
		os.Getenv("PG_URL"),
		os.Getenv("PG_PORT"),
		os.Getenv("PG_DB"))

	pool, err := pgxpool.New(context.Background(), connectionUrl)
	if err != nil {
		log.Printf("Unable to create connection pool: %v\n", err)
		os.Exit(2)
	} else {
		log.Println("Connection to database established")
	}

	log.Println("Checking connection...")
	_, err = pool.Exec(context.Background(), `select * from users limit 1`)
	if err != nil {
		log.Printf("Database error occured: %v\n", err)
		return
	} else {
		log.Println("Database connection checked. Starting server")
	}

	defer pool.Close()

	srcFolder, err := os.Getwd()
	if err != nil {
		log.Printf("Finding directory error: %v\n", err)
		return
	}

	// file server
	imagesPath := path.Dir(strings.Replace(srcFolder, "\\", "/", -1)) + "/public"
	router.PathPrefix("/images").Handler(http.StripPrefix("/images", http.FileServer(http.Dir(imagesPath))))

	// uploading photos
	router.HandleFunc("/api/pictures/profile", security.VerifyJWT(func(w http.ResponseWriter, r *http.Request) {
		fs.LoadPicture(pool, w, r, 3, "profile")
	})).Methods("POST")
	router.HandleFunc("/api/pictures/attachment", security.VerifyJWT(func(w http.ResponseWriter, r *http.Request) {
		fs.LoadPicture(pool, w, r, 10, "attachments")
	})).Methods("POST")

	// auth
	router.HandleFunc("/api/user/login", func(w http.ResponseWriter, r *http.Request) {
		auth.LoginHandler(pool, w, r)
	}).Methods("POST")
	router.HandleFunc("/api/user/register", func(w http.ResponseWriter, r *http.Request) {
		auth.RegisterHandler(pool, w, r)
	}).Methods("POST")

	// general posts
	router.HandleFunc("/api/posts/all", security.VerifyJWT(func(w http.ResponseWriter, r *http.Request) {
		posts.GeneralPostsHandlerGET(pool, w, r)
	})).Methods("GET")
	router.HandleFunc("/api/posts/all", security.VerifyJWT(func(w http.ResponseWriter, r *http.Request) {
		posts.GeneralPostsHandlerPOST(pool, w, r)
	})).Methods("POST")

	// centrifugo
	router.HandleFunc("/centrifugo/publish", func(w http.ResponseWriter, r *http.Request) {
		centrifugo.PubCentrifugoMiddleware(pool, w, r)
	}).Methods("POST")
	router.HandleFunc("/centrifugo/subscribe", func(w http.ResponseWriter, r *http.Request) {
		centrifugo.SubCentrifugoMiddleware(pool, w, r)
	}).Methods("POST")
	router.HandleFunc("/centrifugo/connect", func(w http.ResponseWriter, r *http.Request) {
		centrifugo.ConCentrifugoMiddleware(pool, w, r)
	}).Methods("POST")

	log.Fatal(http.ListenAndServe(":8080", router))
}
