package fs

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/jackc/pgx/v5/pgxpool"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func LoadPicture(pool *pgxpool.Pool, w http.ResponseWriter, r *http.Request, maxSize int64, publicSrc string) {

	if r.Body == http.NoBody {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("No file found!"))
		if err != nil {
			return
		}
	}

	err := r.ParseMultipartForm(maxSize << 20)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Bad file size!"))
		if err != nil {
			return
		}
	}

	uploadFile, handler, err := r.FormFile("image")
	defer uploadFile.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Error Retrieving the file!"))
		if err != nil {
			return
		}
	}

	if path.Dir(handler.Header.Get("Content-type")) != "image" {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Invalid file format!"))
		if err != nil {
			return
		}
	}

	fileUUID := uuid.New()

	fileName := strings.Replace(fileUUID.String(), "-", "", -1)
	fileExt := strings.Split(handler.Filename, ".")[1]
	fileFullName := fmt.Sprintf("%s.%s", fileName, fileExt)

	currDir, _ := os.Getwd()
	dir := path.Dir(strings.Replace(currDir, "\\", "/", -1)) + "/public/" + publicSrc

	localFileFullPath := filepath.Join(dir, fileFullName)

	localFile, err := os.Create(localFileFullPath)
	defer localFile.Close()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Cannot create file!"))
		if err != nil {
			return
		}
	}

	if _, err = io.Copy(localFile, uploadFile); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Cannot copy file!"))
		if err != nil {
			return
		}
	}

	if publicSrc == "profile" {
		userCookie, err := r.Cookie("user_id")
		if err != nil {
			os.Remove(localFileFullPath)
			w.WriteHeader(http.StatusBadRequest)
			_, err := w.Write([]byte("Cookie user_id is not provided"))
			if err != nil {
				return
			}
			return
		}

		userId := userCookie.Value

		query := `
			select (picture_url) from userprofile where id = $1
		`

		var prevPicUrl pgtype.Text
		rows := pool.QueryRow(context.Background(), query, userId)
		err = rows.Scan(&prevPicUrl)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			_, err := w.Write([]byte("Failed to get previous profile image url!"))
			if err != nil {
				return
			}
			return
		}
		if prevPicUrl.Valid {
			file := filepath.Base(prevPicUrl.String)
			err := os.Remove(filepath.Join(dir, file))
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				_, err := w.Write([]byte("Failed to remove previous profile image url!"))
				if err != nil {
					return
				}
			}
		}

		query = `
			update userprofile set picture_url = $1 where id = $2
		`
		_, err = pool.Exec(context.Background(), query, "http://localhost:8080/images/profile/"+fileFullName, userId)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			_, err := w.Write([]byte("Failed to update UserProfile with image"))
			if err != nil {
				return
			}
			return
		}
	}
}
